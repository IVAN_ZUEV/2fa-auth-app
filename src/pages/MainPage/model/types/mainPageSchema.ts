import { User } from '@/entities/User';

export interface MainPageSchema {
    isLoading?: boolean;
    error?: string;
    users?: User[];
}
