import { StateSchema } from '@/app/providers/StoreProvider';

export const getMainPageIsloading = (state: StateSchema) =>
    state.mainPage?.isLoading;
export const getMainPageError = (state: StateSchema) => state.mainPage?.error;
export const getMainPageUsers = (state: StateSchema) => state.mainPage?.users;
