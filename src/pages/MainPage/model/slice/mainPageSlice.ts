import { createSlice } from '@reduxjs/toolkit';

import { fetchUsers } from '@/entities/User';

import { MainPageSchema } from '../types/mainPageSchema';

const initialState: MainPageSchema = {
    error: undefined,
    isLoading: false,
    users: []
};

export const mainPageSlice = createSlice({
    name: 'mainPage',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchUsers.fulfilled, (state, action) => {
                state.users = action.payload;
                state.isLoading = false;
                state.error = undefined;
            })
            .addCase(fetchUsers.pending, (state) => {
                state.isLoading = true;
                state.error = undefined;
            })
            .addCase(fetchUsers.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.payload;
            });
    }
});

export const { actions: mainPageActions } = mainPageSlice;
export const { reducer: mainPageReducer } = mainPageSlice;
