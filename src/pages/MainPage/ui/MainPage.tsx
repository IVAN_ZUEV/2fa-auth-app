import { useEffect } from 'react';
import { useSelector } from 'react-redux';

import { UserList, fetchUsers, getUserAuthData } from '@/entities/User';
import {
    DynamicModuleLoader,
    ReducerList
} from '@/shared/lib/components/DynamicModuleLoader/DynamicModuleLoader';
import { useAppDispatch } from '@/shared/lib/hooks/useAppDispatch/useAppDispatch';
import { VStack } from '@/shared/ui/Stack';
import { Text } from '@/shared/ui/Text';
import { Page } from '@/widgets/Page';

import {
    getMainPageError,
    getMainPageIsloading,
    getMainPageUsers
} from '../model/selectors/mainPage';
import { mainPageReducer } from '../model/slice/mainPageSlice';

import cls from './MainPage.module.scss';

const reducers: ReducerList = {
    mainPage: mainPageReducer
};

const MainPage = () => {
    const dispatch = useAppDispatch();
    const userList = useSelector(getMainPageUsers);
    const isLoading = useSelector(getMainPageIsloading);
    const error = useSelector(getMainPageError);
    const authData = useSelector(getUserAuthData);

    useEffect(() => {
        dispatch(fetchUsers(null));
    }, [dispatch]);

    return (
        <DynamicModuleLoader reducers={reducers}>
            <Page>
                <VStack
                    align="center"
                    justify="center"
                    max
                    className={cls.content}
                >
                    <Text
                        size="XL"
                        weight="bold_weight"
                        className={cls.mainText}
                    >
                        Вы авторизированы в системе!
                    </Text>
                    <VStack gap="12" align="center" max>
                        <Text size="M">{`Ваш email: ${
                            authData?.email || 'Произошла ошибка'
                        }`}</Text>
                    </VStack>

                    <UserList
                        users={userList}
                        isLoading={isLoading}
                        error={error}
                    />
                </VStack>
            </Page>
        </DynamicModuleLoader>
    );
};

export default MainPage;
