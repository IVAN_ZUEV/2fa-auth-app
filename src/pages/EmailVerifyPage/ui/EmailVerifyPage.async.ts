import { lazy } from 'react';

export const EmailVerifyPageAsync = lazy(() => import('./EmailVerifyPage'));
