import { Card, Tag } from 'antd';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { getUserAuthData } from '@/entities/User';
import { getRouteMain } from '@/shared/consts/router';
import { VStack } from '@/shared/ui/Stack';
import { Text } from '@/shared/ui/Text';
import { Page } from '@/widgets/Page';

import cls from './EmailVerifyPage.module.scss';

const EmailVerifyPage = () => {
    const authData = useSelector(getUserAuthData);
    const navigate = useNavigate();

    useEffect(() => {
        if (authData?.isActivated) {
            navigate(getRouteMain());
        }
    }, [authData?.isActivated, navigate]);

    return (
        <Page className={cls.AuthPage}>
            <Card bordered={false} className={cls.card}>
                <VStack align="center" justify="center" gap="16" max>
                    <Text size="L" align="center" weight="bold_weight" max>
                        Подтвердите почту
                    </Text>
                    <Text size="M" align="center" max>
                        Для подтверждения вам необходимо перейти по ссылке в
                        письме на вашей почте
                    </Text>
                    <Tag className={cls.tag} color="processing">
                        Эту страницу можно закрыть
                    </Tag>
                </VStack>
            </Card>
        </Page>
    );
};

export default EmailVerifyPage;
