import { Button, Card } from 'antd';
import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { getUserAuthData } from '@/entities/User';
import { LoginForm } from '@/features/AuthByUsername';
import { RegisterForm } from '@/features/RegisterByUsername';
import { getRouteMain } from '@/shared/consts/router';
import { HStack } from '@/shared/ui/Stack';
import { Page } from '@/widgets/Page';

import cls from './AuthPage.module.scss';

type authMethodType = 'register' | 'login';

const AuthPage = () => {
    const authData = useSelector(getUserAuthData);
    const navigate = useNavigate();
    const [authMethod, setAuthMethod] = useState<authMethodType>('login');

    const onChangeAuthMethod = (authMethod: authMethodType) => {
        setAuthMethod(authMethod);
    };

    useEffect(() => {
        if (authData) {
            navigate(getRouteMain());
        }
    }, [authData, navigate]);

    if (authMethod === 'login') {
        return (
            <Page className={cls.AuthPage}>
                <Card bordered={false} className={cls.card}>
                    <LoginForm />
                    <HStack
                        align="center"
                        justify="center"
                        gap="8"
                        max
                        className={cls.quations}
                    >
                        <p>Еще нет аккаунта? </p>
                        <Button
                            type="text"
                            className={cls.link}
                            onClick={() => onChangeAuthMethod('register')}
                        >
                            Регистрация
                        </Button>
                    </HStack>
                </Card>
            </Page>
        );
    }

    return (
        <Page className={cls.AuthPage}>
            <Card bordered={false} className={cls.card}>
                <RegisterForm />
                <HStack
                    align="center"
                    justify="center"
                    gap="8"
                    max
                    className={cls.quations}
                >
                    <p>Уже есть аккаунт? </p>
                    <Button
                        type="text"
                        className={cls.link}
                        onClick={() => onChangeAuthMethod('login')}
                    >
                        Вход
                    </Button>
                </HStack>
            </Card>
        </Page>
    );
};

export default AuthPage;
