import { createAsyncThunk } from '@reduxjs/toolkit';

import { ThunkConfig } from '@/app/providers/StoreProvider';
import { getUserAuthData } from '@/entities/User';

export interface AddAuthTokensRetunValue {
    qrCode: string;
}

export const addAuthTokens = createAsyncThunk<
    AddAuthTokensRetunValue,
    null,
    ThunkConfig<string>
>('phoneVerify/addAuthTokens', async (_, thunkApi) => {
    const { extra, rejectWithValue, getState } = thunkApi;
    const user = getUserAuthData(getState());

    try {
        const response = await extra.api.post<AddAuthTokensRetunValue>(
            '/secret/qrcode',
            {
                userId: user?.id
            }
        );

        if (!response.data) {
            throw new Error();
        }

        return response.data;
    } catch (e) {
        return rejectWithValue('error');
    }
});
