import { createAsyncThunk } from '@reduxjs/toolkit';

import { ThunkConfig } from '@/app/providers/StoreProvider';
import { getUserAuthData } from '@/entities/User';

export interface VerifyTokenRetunValue {
    verified: boolean;
}

export const verifyToken = createAsyncThunk<
    VerifyTokenRetunValue,
    string,
    ThunkConfig<string>
>('phoneVerify/verifyToken', async (token, thunkApi) => {
    const { dispatch, extra, rejectWithValue, getState } = thunkApi;
    const user = getUserAuthData(getState());

    try {
        const response = await extra.api.post<VerifyTokenRetunValue>(
            '/phone/verify',
            {
                token,
                userId: user?.id
            }
        );

        if (!response.data) {
            throw new Error();
        }

        return response.data;
    } catch (e) {
        return rejectWithValue('error');
    }
});
