import { PayloadAction, createSlice } from '@reduxjs/toolkit';

import { addAuthTokens } from '../services/addAuthTokens';
import { verifyToken } from '../services/verifyToken';
import { PhoneVerifySchema } from '../types/phoneVerify';

const initialState: PhoneVerifySchema = {
    error: undefined,
    isLoading: false,
    qrCode: undefined,
    token: '',
    isSuccess: false
};

export const phoneVerifySlice = createSlice({
    name: 'phoneVerify',
    initialState,
    reducers: {
        setToken: (state, action: PayloadAction<string>) => {
            state.token = action.payload;
            state.error = undefined;
        },
        setIsSuccess: (state, action: PayloadAction<boolean>) => {
            state.isSuccess = action.payload;
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(addAuthTokens.fulfilled, (state, action) => {
                state.qrCode = action.payload.qrCode;
                state.isLoading = false;
                state.error = undefined;
            })
            .addCase(addAuthTokens.pending, (state) => {
                state.isLoading = true;
                state.error = undefined;
            })
            .addCase(addAuthTokens.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.payload;
            })
            .addCase(verifyToken.fulfilled, (state, action) => {
                state.isSuccess = true;
                state.isLoading = false;
                state.error = undefined;
            })
            .addCase(verifyToken.pending, (state) => {
                state.isLoading = true;
                state.error = undefined;
            })
            .addCase(verifyToken.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.payload;
            });
    }
});

export const { actions: phoneVerifyActions } = phoneVerifySlice;
export const { reducer: phoneVerifyReducer } = phoneVerifySlice;
