export interface PhoneVerifySchema {
    token: string;
    qrCode?: string;
    isLoading?: boolean;
    error?: string;
    isSuccess?: boolean;
}
