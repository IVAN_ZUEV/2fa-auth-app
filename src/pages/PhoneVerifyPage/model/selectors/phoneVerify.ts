import { StateSchema } from '@/app/providers/StoreProvider';

export const getPhoneVerifyToken = (state: StateSchema) =>
    state.phoneVerify?.token;
export const getPhoneVerifyIsLoading = (state: StateSchema) =>
    state.phoneVerify?.isLoading;
export const getPhoneVerifyError = (state: StateSchema) =>
    state.phoneVerify?.error;
export const getPhoneVerifyIsSuccess = (state: StateSchema) =>
    state.phoneVerify?.isSuccess;
export const getPhoneVerifyQrCode = (state: StateSchema) =>
    state.phoneVerify?.qrCode;
