import { Button, Card, QRCode } from 'antd';
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

import { getUserAuthData } from '@/entities/User';
import { getRouteMain } from '@/shared/consts/router';
import { useAppDispatch } from '@/shared/lib/hooks/useAppDispatch/useAppDispatch';
import { Input } from '@/shared/ui/Input';
import { HStack, VStack } from '@/shared/ui/Stack';
import { Text } from '@/shared/ui/Text';
import { Page } from '@/widgets/Page';

import {
    getPhoneVerifyError,
    getPhoneVerifyIsLoading,
    getPhoneVerifyIsSuccess,
    getPhoneVerifyQrCode,
    getPhoneVerifyToken
} from '../model/selectors/phoneVerify';
import { addAuthTokens } from '../model/services/addAuthTokens';
import { verifyToken } from '../model/services/verifyToken';
import { phoneVerifyActions } from '../model/slice/phoneVerifySlice';

import cls from './PhoneVerifyPage.module.scss';

const PhoneVerifyPage = () => {
    const dispatch = useAppDispatch();
    const authData = useSelector(getUserAuthData);
    const navigate = useNavigate();

    const qrCode = useSelector(getPhoneVerifyQrCode);
    const token = useSelector(getPhoneVerifyToken);
    const isLoading = useSelector(getPhoneVerifyIsLoading);
    const error = useSelector(getPhoneVerifyError);
    const isSuccess = useSelector(getPhoneVerifyIsSuccess);

    useEffect(() => {
        if (!authData?.is2FA) {
            dispatch(addAuthTokens(null));
        }
    }, [authData?.is2FA, dispatch]);

    useEffect(() => {
        if (isSuccess) {
            navigate(getRouteMain());
        }
    }, [isSuccess, navigate]);

    const onChangeCode = (value: string) => {
        dispatch(phoneVerifyActions.setToken(value));
    };

    const onVerifyToken = () => {
        dispatch(verifyToken(token || ''));
    };

    if (authData?.is2FA) {
        return (
            <Page className={cls.AuthPage}>
                <Card bordered={false} className={cls.card}>
                    <VStack align="center" justify="center" gap="32">
                        <HStack gap="8" className={cls.inputWrapper}>
                            <Input
                                placeholder="Введите код"
                                value={token}
                                onChange={onChangeCode}
                                isError={!!error}
                                className={cls.input}
                            />
                            <Button
                                disabled={!token || token?.length < 6}
                                type="primary"
                                className={cls.btn}
                                onClick={onVerifyToken}
                            >
                                Отправить
                            </Button>
                        </HStack>
                        {error && <Text theme="error">Неверный код!</Text>}
                        <VStack align="center" justify="center" gap="4" max>
                            <Text size="LARGE_S" max align="center">
                                1. Зайдите в приложение Google Authenticator
                            </Text>
                            <Text size="LARGE_S" max align="center">
                                2. Введите одноразовый код
                            </Text>
                        </VStack>
                    </VStack>
                </Card>
            </Page>
        );
    }

    return (
        <Page className={cls.AuthPage}>
            <Card bordered={false} className={cls.card}>
                <VStack align="center" justify="center" gap="32">
                    <QRCode
                        type="svg"
                        value={qrCode || '-'}
                        status={isLoading ? 'loading' : 'active'}
                        size={258}
                    />
                    <HStack gap="8" className={cls.inputWrapper}>
                        <Input
                            placeholder="Введите код"
                            value={token}
                            onChange={onChangeCode}
                            className={cls.input}
                        />
                        <Button
                            disabled={!token || token?.length < 6}
                            type="primary"
                            className={cls.btn}
                            onClick={onVerifyToken}
                        >
                            Отправить
                        </Button>
                    </HStack>
                    {error && <Text theme="error">Неверный код!</Text>}
                    <VStack align="center" justify="center" gap="4" max>
                        <Text size="LARGE_S">
                            1. Отсканируйте QR-код в приложении Google
                            Authenticator
                        </Text>
                        <Text size="LARGE_S">
                            2. Введите одноразовый код из приложения
                        </Text>
                    </VStack>
                </VStack>
            </Card>
        </Page>
    );
};

export default PhoneVerifyPage;
