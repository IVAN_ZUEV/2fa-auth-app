import { lazy } from 'react';

export const PhoneVerifyPageAsync = lazy(() => import('./PhoneVerifyPage'));
