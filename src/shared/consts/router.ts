export enum AppRoutes {
    MAIN = '/',
    AUTH = '/auth',
    EMAIL_VERIFY = '/email/verify',
    PHONE_VERIFY = '/phone/verify',
    // last page
    NOT_FOUND = 'not_found'
}

export const getRouteMain = () => '/';
export const getRouteAuth = () => '/auth';
export const getRouteEmailVerify = () => '/email/verify';
export const getRoutePhoneVerify = () => '/phone/verify';
export const getRouteNotFound = () => '*';
