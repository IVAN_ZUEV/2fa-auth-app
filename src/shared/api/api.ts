import axios from 'axios';

// eslint-disable-next-line babun4ek-fsd-plugin/layer-imports-checker
import { AuthResponse } from '@/entities/User';

export const $api = axios.create({
    withCredentials: true,
    baseURL: __API__,
    headers: {
        'Content-Type': 'application/json; charset=utf-8'
    }
});

$api.interceptors.request.use((config) => {
    if (config.headers) {
        config.headers.Authorization = `Bearer ${localStorage.getItem(
            'token'
        )}`;
    }

    return config;
});

$api.interceptors.response.use(
    (config) => config,
    async (error) => {
        const originalRequest = error.config;
        if (
            error.response.status === 401 &&
            error.config &&
            !error.config._isRetry
        ) {
            originalRequest._isRetry = true;
            try {
                const response = await axios.get<AuthResponse>(
                    `${__API__}/refresh`,
                    { withCredentials: true }
                );
                if (response.data.accessToken) {
                    localStorage.setItem('token', response.data.accessToken);
                }
                return $api.request(originalRequest);
            } catch (e) {
                localStorage.removeItem('token');
                console.log('НЕ АВТОРИЗОВАН');
            }
        }
        throw error;
    }
);
