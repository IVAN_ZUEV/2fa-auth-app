import { useEffect } from 'react';
import { useSelector } from 'react-redux';

import { checkAuth, getUserIsLoading } from '@/entities/User';
import { TOKEN_LOCALSTORAGE_KEY } from '@/shared/consts/localStorage';
import { useAppDispatch } from '@/shared/lib/hooks/useAppDispatch/useAppDispatch';
import { Navbar } from '@/widgets/Navbar';
import { PageLoader } from '@/widgets/PageLoader';

import { AppRouter } from './providers/router';

import './styles/index.scss';

const App = () => {
    const dispatch = useAppDispatch();
    const isLoading = useSelector(getUserIsLoading);

    useEffect(() => {
        if (localStorage.getItem(TOKEN_LOCALSTORAGE_KEY)) {
            dispatch(checkAuth(null));
        }
    }, [dispatch]);

    return (
        <div className="app">
            <Navbar />
            <div className="content-page">
                {isLoading ? <PageLoader /> : <AppRouter />}
            </div>
        </div>
    );
};

export default App;
