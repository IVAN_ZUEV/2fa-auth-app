import { memo, Suspense, useCallback } from 'react';
import { Route, Routes, RouteProps } from 'react-router-dom';

import { PageLoader } from '@/widgets/PageLoader';

import { routeConfig } from '../config/routeConfig';

import { RequirePhoneVerify } from './RequiewPhoneVerify';
import { RequireAuth } from './RequireAuth';
import { RequireEmailVerify } from './RequireEmailVerify';

const AppRouter = () => {
    const renderWithWrapper = useCallback((route: RouteProps) => {
        const element = (
            <RequireAuth>
                <RequireEmailVerify>
                    <RequirePhoneVerify>
                        <Suspense fallback={<PageLoader />}>
                            {route.element}
                        </Suspense>
                    </RequirePhoneVerify>
                </RequireEmailVerify>
            </RequireAuth>
        );
        return <Route key={route.path} path={route.path} element={element} />;
    }, []);

    return <Routes>{Object.values(routeConfig).map(renderWithWrapper)}</Routes>;
};

export default memo(AppRouter);
