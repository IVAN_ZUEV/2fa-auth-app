import { useSelector } from 'react-redux';
import { Navigate, useLocation } from 'react-router-dom';

import { getUserAuthData } from '@/entities/User';
import { getPhoneVerifyIsSuccess } from '@/pages/PhoneVerifyPage';
import { getRoutePhoneVerify } from '@/shared/consts/router';

export function RequirePhoneVerify({ children }: { children: JSX.Element }) {
    const authData = useSelector(getUserAuthData);
    const location = useLocation();
    const isVerify = useSelector(getPhoneVerifyIsSuccess);

    if (
        authData &&
        authData.isActivated &&
        !isVerify &&
        location.pathname !== getRoutePhoneVerify()
    ) {
        return <Navigate to={getRoutePhoneVerify()} replace />;
    }

    return children;
}
