import { useSelector } from 'react-redux';
import { Navigate, useLocation } from 'react-router-dom';

import { getUserAuthData } from '@/entities/User';
import { TOKEN_LOCALSTORAGE_KEY } from '@/shared/consts/localStorage';
import { getRouteEmailVerify } from '@/shared/consts/router';

export function RequireEmailVerify({ children }: { children: JSX.Element }) {
    const authData = useSelector(getUserAuthData);
    const location = useLocation();
    const isAuth = localStorage.getItem(TOKEN_LOCALSTORAGE_KEY);

    if (
        isAuth &&
        authData &&
        !authData?.isActivated &&
        location.pathname !== getRouteEmailVerify()
    ) {
        return <Navigate to={getRouteEmailVerify()} replace />;
    }

    return children;
}
