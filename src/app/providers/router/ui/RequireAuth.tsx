import { Navigate, useLocation } from 'react-router-dom';

import { TOKEN_LOCALSTORAGE_KEY } from '@/shared/consts/localStorage';
import { getRouteAuth } from '@/shared/consts/router';

export function RequireAuth({ children }: { children: JSX.Element }) {
    const isAuth = localStorage.getItem(TOKEN_LOCALSTORAGE_KEY);
    const location = useLocation();

    if (!isAuth && location.pathname !== getRouteAuth()) {
        return <Navigate to={getRouteAuth()} replace />;
    }

    return children;
}
