import { RouteProps } from 'react-router-dom';

import { AuthPage } from '@/pages/AuthPage';
import { EmailVerifyPage } from '@/pages/EmailVerifyPage';
import { MainPage } from '@/pages/MainPage';
import { NotFoundPage } from '@/pages/NotFoundPage';
import { PhoneVerifyPage } from '@/pages/PhoneVerifyPage';
import {
    AppRoutes,
    getRouteAuth,
    getRouteMain,
    getRouteNotFound,
    getRouteEmailVerify,
    getRoutePhoneVerify
} from '@/shared/consts/router';

export const routeConfig: Record<AppRoutes, RouteProps> = {
    [AppRoutes.MAIN]: {
        path: getRouteMain(),
        element: <MainPage />
    },
    [AppRoutes.AUTH]: {
        path: getRouteAuth(),
        element: <AuthPage />
    },
    [AppRoutes.EMAIL_VERIFY]: {
        path: getRouteEmailVerify(),
        element: <EmailVerifyPage />
    },
    [AppRoutes.PHONE_VERIFY]: {
        path: getRoutePhoneVerify(),
        element: <PhoneVerifyPage />
    },

    // last route
    [AppRoutes.NOT_FOUND]: {
        path: getRouteNotFound(),
        element: <NotFoundPage />
    }
};
