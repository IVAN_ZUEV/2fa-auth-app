import {
    AnyAction,
    CombinedState,
    EnhancedStore,
    Reducer,
    ReducersMapObject
} from '@reduxjs/toolkit';
import { AxiosInstance } from 'axios';

import { UserSchema } from '@/entities/User';
import { LoginSchema } from '@/features/AuthByUsername';
import { RegisterSchema } from '@/features/RegisterByUsername';
import { MainPageSchema } from '@/pages/MainPage';
import { PhoneVerifySchema } from '@/pages/PhoneVerifyPage';

export interface StateSchema {
    user: UserSchema;
    phoneVerify: PhoneVerifySchema;

    // асинхронные редюсеры
    mainPage?: MainPageSchema;
    loginForm?: LoginSchema;
    registerForm?: RegisterSchema;
}

export type StateSchemaKey = keyof StateSchema;

export interface ReducerManager {
    getReducerMap: () => ReducersMapObject<StateSchema>;
    reduce: (
        state: StateSchema,
        action: AnyAction
    ) => CombinedState<StateSchema>;
    add: (key: StateSchemaKey, reducer: Reducer) => void;
    remove: (key: StateSchemaKey) => void;
}

export interface ReduxStoreWithManager extends EnhancedStore<StateSchema> {
    reducerManager: ReducerManager;
}

export interface ThunkExtraArg {
    api: AxiosInstance;
}

export interface ThunkConfig<T> {
    rejectValue: T;
    extra: ThunkExtraArg;
    state: StateSchema;
}
