import { ConfigProvider } from 'antd';
import locale from 'antd/locale/ru_RU';
import { ReactNode } from 'react';

import 'dayjs/locale/ru';

interface AntdProviderProps {
    children: ReactNode;
}

export const AntdProvider = ({ children }: AntdProviderProps) => (
    <ConfigProvider
        locale={locale}
        theme={{
            token: {
                fontSize: 13,
                // @ts-ignore
                fontFamily: '\'Montserrat\', sans-serif'
            }
        }}
    >
        {children}
    </ConfigProvider>
);
