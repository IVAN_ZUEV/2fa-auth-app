import { Button } from 'antd';
import { useCallback } from 'react';
import { useSelector } from 'react-redux';

import {
    DynamicModuleLoader,
    ReducerList
} from '@/shared/lib/components/DynamicModuleLoader/DynamicModuleLoader';
import { useAppDispatch } from '@/shared/lib/hooks/useAppDispatch/useAppDispatch';
import { Input } from '@/shared/ui/Input';
import { VStack } from '@/shared/ui/Stack';
import { Text } from '@/shared/ui/Text';

import {
    getLoginError,
    getLoginIsLoading,
    getLoginPassword,
    getLoginEmail
} from '../../model/selectors/login';
import { loginByUsername } from '../../model/services/loginByUsername/loginByUsername';
import { loginActions, loginReducer } from '../../model/slice/loginSlice';

import cls from './LoginForm.module.scss';

const title: string = 'Авторизация';

const initialReducers: ReducerList = {
    loginForm: loginReducer
};

const LoginForm = () => {
    const email = useSelector(getLoginEmail);
    const password = useSelector(getLoginPassword);
    const isLoading = useSelector(getLoginIsLoading);
    const error = useSelector(getLoginError);
    const isDisabled = !password || !email;
    const dispatch = useAppDispatch();

    const onChangeEmail = useCallback(
        (value: string) => {
            dispatch(loginActions.setEmail(value));
        },
        [dispatch]
    );

    const onChangePassword = useCallback(
        (value: string) => {
            dispatch(loginActions.setPassword(value));
        },
        [dispatch]
    );

    const onEnter = useCallback(() => {
        dispatch(
            loginByUsername({
                email: email.trim(),
                password: password.trim()
            })
        );
    }, [dispatch, email, password]);

    return (
        <DynamicModuleLoader reducers={initialReducers} removeAfterAnmount>
            <Text weight="bold_weight" className={cls.logo}>
                2FA-Auth
            </Text>
            <VStack justify="center" gap="12" className={cls.content} max>
                <Text size="M" weight="bold_weight" align="center" max>
                    {title}
                </Text>
                {error && (
                    <Text align="center" theme="error" max>
                        Не верный логин или пароль
                    </Text>
                )}
                <VStack gap="4" max>
                    <Text className={cls.subText}>Email</Text>
                    <Input
                        placeholder="Ваш email"
                        value={email}
                        isError={!!error}
                        onChange={onChangeEmail}
                    />
                </VStack>
                <VStack gap="4" max>
                    <Text className={cls.subText}>Пароль</Text>
                    <Input
                        placeholder="Ваш пароль"
                        value={password}
                        isError={!!error}
                        onChange={onChangePassword}
                    />
                </VStack>

                <Button
                    type="primary"
                    className={cls.btn}
                    onClick={onEnter}
                    loading={isLoading}
                    disabled={isDisabled}
                >
                    Войти
                </Button>
            </VStack>
        </DynamicModuleLoader>
    );
};

export default LoginForm;
