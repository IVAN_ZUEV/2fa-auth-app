export { RegisterFormAsync as RegisterForm } from './ui/RegisterForm/RegisterForm.async';
export type { RegisterSchema } from './model/types/RegisterSchema';
