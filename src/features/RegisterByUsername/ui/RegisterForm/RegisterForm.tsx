import { Button } from 'antd';
import { useCallback } from 'react';
import { useSelector } from 'react-redux';

import {
    DynamicModuleLoader,
    ReducerList
} from '@/shared/lib/components/DynamicModuleLoader/DynamicModuleLoader';
import { useAppDispatch } from '@/shared/lib/hooks/useAppDispatch/useAppDispatch';
import { Input } from '@/shared/ui/Input';
import { VStack } from '@/shared/ui/Stack';
import { Text } from '@/shared/ui/Text';

import {
    getRegisterEmail,
    getRegisterError,
    getRegisterIsLoading,
    getRegisterPassword
} from '../../model/selectors/register';
import { registerByUsername } from '../../model/services/registerByUsername/registerByUsername';
import {
    registerActions,
    registerReducer
} from '../../model/slice/registerSlice';

import cls from './RegisterForm.module.scss';

const title: string = 'Регистрация';

const initialReducers: ReducerList = {
    registerForm: registerReducer
};

const RegisterForm = () => {
    const email = useSelector(getRegisterEmail);
    const password = useSelector(getRegisterPassword);
    const isLoading = useSelector(getRegisterIsLoading);
    const error = useSelector(getRegisterError);
    const isDisabled = !password || !email;
    const dispatch = useAppDispatch();

    const onChangeEmail = useCallback(
        (value: string) => {
            dispatch(registerActions.setEmail(value));
        },
        [dispatch]
    );

    const onChangePassword = useCallback(
        (value: string) => {
            dispatch(registerActions.setPassword(value));
        },
        [dispatch]
    );

    const onRegister = useCallback(() => {
        dispatch(
            registerByUsername({
                email: email.trim(),
                password: password.trim()
            })
        );
    }, [dispatch, password, email]);

    return (
        <DynamicModuleLoader reducers={initialReducers} removeAfterAnmount>
            <Text weight="bold_weight" className={cls.logo}>
                2FA-Auth
            </Text>
            <VStack justify="center" gap="12" className={cls.content} max>
                <Text size="M" weight="bold_weight" align="center" max>
                    {title}
                </Text>
                {error && (
                    <Text align="center" theme="error" max>
                        Не верный логин или пароль
                    </Text>
                )}
                <VStack gap="4" max>
                    <Text className={cls.subText}>Введите ваш email</Text>
                    <Input
                        placeholder="Ваш email"
                        value={email}
                        isError={!!error}
                        onChange={onChangeEmail}
                    />
                </VStack>
                <VStack gap="4" max>
                    <Text className={cls.subText}>Придумайте пароль</Text>
                    <Input
                        placeholder="Ваш пароль"
                        value={password}
                        isError={!!error}
                        onChange={onChangePassword}
                    />
                </VStack>

                <Button
                    type="primary"
                    className={cls.btn}
                    onClick={onRegister}
                    loading={isLoading}
                    disabled={isDisabled}
                >
                    Зарегистрироваться
                </Button>
            </VStack>
        </DynamicModuleLoader>
    );
};

export default RegisterForm;
