import { createAsyncThunk } from '@reduxjs/toolkit';

import { ThunkConfig } from '@/app/providers/StoreProvider';
import { AuthResponse, userActions } from '@/entities/User';
import { TOKEN_LOCALSTORAGE_KEY } from '@/shared/consts/localStorage';

export interface RegisterByUsernameProps {
    email: string;
    password: string;
}

export const registerByUsername = createAsyncThunk<
    AuthResponse,
    RegisterByUsernameProps,
    ThunkConfig<string>
>('user/registerByUsername', async (authData, thunkApi) => {
    const { dispatch, extra, rejectWithValue } = thunkApi;

    try {
        const response = await extra.api.post<AuthResponse>(
            '/register',
            authData
        );

        if (!response.data) {
            throw new Error();
        }

        console.log('[REGISTER]: ', response.data);

        localStorage.setItem(TOKEN_LOCALSTORAGE_KEY, response.data.accessToken);
        dispatch(userActions.setAuthData(response.data.user));

        return response.data;
    } catch (e) {
        return rejectWithValue('error');
    }
});
