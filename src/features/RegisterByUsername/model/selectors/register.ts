import { StateSchema } from '@/app/providers/StoreProvider';

export const getRegisterError = (state: StateSchema) =>
    state?.registerForm?.error;
export const getRegisterIsLoading = (state: StateSchema) =>
    state?.registerForm?.isLoading;
export const getRegisterPassword = (state: StateSchema) =>
    state?.registerForm?.password || '';
export const getRegisterEmail = (state: StateSchema) =>
    state?.registerForm?.email || '';
