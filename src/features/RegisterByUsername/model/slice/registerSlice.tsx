import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { registerByUsername } from '../services/registerByUsername/registerByUsername';
import { RegisterSchema } from '../types/RegisterSchema';

const initialState: RegisterSchema = {
    email: '',
    password: '',
    isLoading: false
};

export const registerSlice = createSlice({
    name: 'register',
    initialState,
    reducers: {
        setEmail: (state, action: PayloadAction<string>) => {
            state.email = action.payload;
            state.error = undefined;
        },
        setPassword: (state, action: PayloadAction<string>) => {
            state.password = action.payload;
            state.error = undefined;
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(registerByUsername.pending, (state) => {
                state.error = undefined;
                state.isLoading = true;
            })
            .addCase(registerByUsername.fulfilled, (state, action) => {
                state.isLoading = false;
            })
            .addCase(registerByUsername.rejected, (state, action) => {
                state.isLoading = false;
                state.error = action.payload;
            });
    }
});

export const { actions: registerActions } = registerSlice;
export const { reducer: registerReducer } = registerSlice;
