import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { TOKEN_LOCALSTORAGE_KEY } from '@/shared/consts/localStorage';

import { checkAuth } from '../services/checkAuth';
import { User, UserSchema } from '../types/user';

const initialState: UserSchema = {
    _inited: false,
    isLoading: false
};

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        setAuthData: (state, action: PayloadAction<User>) => {
            state.authData = action.payload;
        },
        initAuthData: (state) => {
            state._inited = true;
        },
        logout: (state) => {
            state.authData = undefined;
            localStorage.removeItem(TOKEN_LOCALSTORAGE_KEY);
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(checkAuth.fulfilled, (state, action) => {
                state.isLoading = false;
            })
            .addCase(checkAuth.pending, (state) => {
                state._inited = true;
                state.isLoading = true;
            })
            .addCase(checkAuth.rejected, (state) => {
                state.isLoading = false;
            });
    }
});

export const { actions: userActions } = userSlice;
export const { reducer: userReducer } = userSlice;
