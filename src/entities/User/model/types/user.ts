export interface User {
    id: string;
    email: string;
    isActivated: boolean;
    is2FA: boolean;
}

export interface UserSchema {
    authData?: User;
    _inited: boolean;
    isLoading?: boolean;
}

export interface AuthResponse {
    accessToken: string;
    refreshToken: string;
    user: User;
    isVerify?: boolean;
}
