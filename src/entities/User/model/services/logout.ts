import { createAsyncThunk } from '@reduxjs/toolkit';
import { AxiosResponse } from 'axios';

import { ThunkConfig } from '@/app/providers/StoreProvider';
// eslint-disable-next-line babun4ek-fsd-plugin/layer-imports-checker
import { phoneVerifyActions } from '@/pages/PhoneVerifyPage';

import { userActions } from '../slice/userSlice';
import { User } from '../types/user';

export const logout = createAsyncThunk<User, null, ThunkConfig<string>>(
    'user/logout',
    async (_, thunkApi) => {
        const { extra, rejectWithValue, dispatch } = thunkApi;

        try {
            dispatch(userActions.logout());
            dispatch(phoneVerifyActions.setIsSuccess(false));

            const response: AxiosResponse<User> = await extra.api.post(
                '/logout'
            );

            if (!response.data) {
                throw new Error(response.data);
            }

            return response.data;
        } catch (e) {
            return rejectWithValue('error');
        }
    }
);
