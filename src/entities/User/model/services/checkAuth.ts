import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

import { ThunkConfig } from '@/app/providers/StoreProvider';
// eslint-disable-next-line babun4ek-fsd-plugin/layer-imports-checker
import { phoneVerifyActions } from '@/pages/PhoneVerifyPage';
import { TOKEN_LOCALSTORAGE_KEY } from '@/shared/consts/localStorage';

import { userActions } from '../slice/userSlice';
import { AuthResponse } from '../types/user';

export const checkAuth = createAsyncThunk<
    AuthResponse,
    null,
    ThunkConfig<string>
>('user/checkAuth', async (_, thunkApi) => {
    const { rejectWithValue, dispatch } = thunkApi;

    try {
        const response = await axios.get<AuthResponse>(`${__API__}/refresh`, {
            withCredentials: true
        });

        if (response.data.accessToken) {
            localStorage.setItem(
                TOKEN_LOCALSTORAGE_KEY,
                response.data.accessToken
            );
        }

        if (!response.data) {
            throw new Error(response.data);
        }

        dispatch(userActions.setAuthData(response.data.user));
        dispatch(
            phoneVerifyActions.setIsSuccess(response.data.isVerify || false)
        );

        return response.data;
    } catch (e) {
        return rejectWithValue('error');
    }
});
