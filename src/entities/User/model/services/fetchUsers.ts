import { createAsyncThunk } from '@reduxjs/toolkit';
import { AxiosResponse } from 'axios';

import { ThunkConfig } from '@/app/providers/StoreProvider';

import { User } from '../types/user';

export const fetchUsers = createAsyncThunk<User[], null, ThunkConfig<string>>(
    'user/fetchUsers',
    async (_, thunkApi) => {
        const { extra, rejectWithValue, dispatch } = thunkApi;

        try {
            const response: AxiosResponse<User[]> = await extra.api.get(
                '/users'
            );

            if (!response.data) {
                throw new Error(response.data);
            }

            return response.data;
        } catch (e) {
            return rejectWithValue('error');
        }
    }
);
