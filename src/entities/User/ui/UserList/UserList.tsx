import { List, Spin } from 'antd';

import { VStack } from '@/shared/ui/Stack';
import { Text } from '@/shared/ui/Text';

import { User } from '../../model/types/user';

import cls from './UserList.module.scss';

interface UserListProps {
    users?: User[];
    isLoading?: boolean;
    error?: string;
}

export const UserList = (props: UserListProps) => {
    const { users, error, isLoading } = props;

    if (error) {
        return (
            <Text theme="error">
                Вы не можете больше получать доступ к этой информации. Обновите
                страницу и попробуйте снова
            </Text>
        );
    }

    if (isLoading) {
        return <Spin className={cls.spinner} size="large" />;
    }

    return (
        <VStack
            align="start"
            justify="center"
            className={cls.listWrap}
            gap="16"
        >
            <Text size="M" max align="center">
                Список зарегистрированных пользователей:
            </Text>
            <List
                bordered
                dataSource={users}
                renderItem={(item) => (
                    <List.Item>
                        <Text>{item.email}</Text>
                    </List.Item>
                )}
                className={cls.list}
            />
        </VStack>
    );
};
