import { DownOutlined, LogoutOutlined } from '@ant-design/icons';
import { Button, Dropdown, MenuProps, Space } from 'antd';
import { memo } from 'react';
import { useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';

import { getUserAuthData, logout } from '@/entities/User';
import { getRouteAuth, getRouteMain } from '@/shared/consts/router';
import { classNames } from '@/shared/lib/helpers/classNames';
import { useAppDispatch } from '@/shared/lib/hooks/useAppDispatch/useAppDispatch';
import { HStack } from '@/shared/ui/Stack';
import { Text } from '@/shared/ui/Text';

import cls from './Navbar.module.scss';

interface NavbarProps {
    className?: string;
}

export const Navbar = memo(({ className }: NavbarProps) => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();
    const userData = useSelector(getUserAuthData);

    const items = [
        {
            key: '1',
            label: 'Выйти',
            icon: <LogoutOutlined />,
            danger: true
        }
    ];

    const onLogout = () => {
        dispatch(logout(null));
        navigate(getRouteAuth());
    };

    const handleMenuClick: MenuProps['onClick'] = (e) => {
        if (
            items.find((item) => item?.key === e.key && item.label === 'Выйти')
        ) {
            onLogout();
        }
    };

    const menuProps = {
        items,
        onClick: handleMenuClick
    };

    return (
        <nav className={classNames(cls.Navbar, {}, [className])}>
            <HStack gap="4" justify="center" align="center">
                <Link to={getRouteMain()}>
                    <Text size="L" weight="bold_weight" className={cls.logo}>
                        2FA-Auth
                    </Text>
                </Link>
            </HStack>

            {userData && (
                <HStack align="center" gap="12" className={cls.logout}>
                    <Dropdown menu={menuProps} trigger={['click']}>
                        <Button>
                            <Space>
                                <Text>{userData.email}</Text>
                                <DownOutlined />
                            </Space>
                        </Button>
                    </Dropdown>
                </HStack>
            )}
        </nav>
    );
});
